package com.hystrix.hystrixclient;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableEurekaClient
@EnableCircuitBreaker
@RestController
public class HystrixClientApplication {
    private RestTemplate restTemplate;

    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }


    @Bean
    @LoadBalanced
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }

    public static void main(String[] args) {
        SpringApplication.run(HystrixClientApplication.class, args);
    }


    @GetMapping("/test")
    @HystrixCommand(fallbackMethod = "myFallback")
    public String test(){
        return restTemplate.getForObject("http://eureka-client/greeting", String.class);
    }


    @GetMapping("/msg")
    @HystrixCommand(fallbackMethod = "myFallback")
    public String msg(){
        return restTemplate.getForObject("http://eureka-client/msg", String.class);
    }

    public String myFallback(){
        return "Где-то что-то упало...";
    }


}
