package com.client.eurekaclient;

import org.springframework.web.bind.annotation.RequestMapping;

public interface GreetingController {

    @RequestMapping("/greeting")
    String greeting();

    @RequestMapping("/msg")
    String msg();

}
