package com.example.feignclienteureka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class GreetingController {


    private GreetingClient greetingClient;

    @Autowired
    public void setGreetingClient(GreetingClient greetingClient) {
        this.greetingClient = greetingClient;
    }

    @GetMapping("/get-greeting")
    public String greeting(Model model){
        String greeting = greetingClient.greeting();
        model.addAttribute("greeting",greeting);
        return "greeting";
    }

    @GetMapping("/msg")
    public String msg(Model model){
        String msg = greetingClient.msg();
        model.addAttribute("msg",msg);
        return "msg";
    }
}
