package com.example.feignclienteureka;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient("eureka-client")
public interface GreetingClient {

    @RequestMapping("/greeting")
    String greeting();

    @GetMapping("/msg")
    String msg();

}
